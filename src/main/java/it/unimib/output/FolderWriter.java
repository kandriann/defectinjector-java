package it.unimib.output;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FolderWriter {

    public static void copyInputFolder(Path srcFolderPath, boolean isOutputEntireFolder, Path uniqueFolderPath){
        Path destFolderPath = Paths.get(uniqueFolderPath.toString()).resolve(srcFolderPath.getFileName());

        File srcFolder = srcFolderPath.toFile();
        File destFolder = destFolderPath.toFile();

        if (isOutputEntireFolder) {
            try {
                FileUtils.copyDirectory(srcFolder, destFolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Path createUniqueFolder(String option, String jsonFileName) throws IOException {
        Path uniqueFolderPath = Paths.get("output_chatbot_folder", "[" + getTimeStamp() + "] - " + jsonFileName + " - " + option);
        if (!Files.exists(uniqueFolderPath)) {
            Files.createDirectories(uniqueFolderPath);
        }
        return uniqueFolderPath;
    }

    private static String getTimeStamp() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        return formatter.format(date);
    }
}
