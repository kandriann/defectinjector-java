package it.unimib.output;

import java.io.IOException;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

public class LogWriter {
    private static Logger logger;
    public static void setLogFile(Path uniqueFolderPath, String jsonFileName) throws IOException {
        logger = Logger.getLogger("");

        String logFileName = uniqueFolderPath + "/log_" + getTimeStamp() + "_" + jsonFileName + ".txt";
        FileHandler fileHandler = new FileHandler(logFileName);

        // Rimuovi tutti i FileHandler precedenti
        Logger logger = Logger.getLogger("");  // Ottieni il logger radice
        Handler[] handlers = logger.getHandlers();
        for (Handler handler : handlers) {
            if (handler instanceof FileHandler) {
                FileHandler previousFileHandler = (FileHandler) handler;
                previousFileHandler.close();  // Chiudi il file di log precedente
                logger.removeHandler(handler);
            }
        }

        // Configurazione del formato del log personalizzato
        SimpleFormatter formatter = new SimpleFormatter() {
            private final String format = "[%1$tT] %2$s %n";

            @Override
            public synchronized String format(LogRecord lr) {
                return String.format(format,
                        new java.util.Date(lr.getMillis()),
                        lr.getMessage()
                );
            }
        };

        // Setto il formatter e il livello di logging
        fileHandler.setFormatter(formatter);
        fileHandler.setLevel(Level.INFO);
        logger.addHandler(fileHandler);
    }

    public static void writeLog(String message) {
        logger.info(message);
    }

    public static void writeErrorLog(Level level, String text, String message) {
        logger.log(level, text, message);
    }

    private static String getTimeStamp() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        return formatter.format(date);
    }
}
