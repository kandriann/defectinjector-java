package it.unimib.output;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ReportWriter {
    private static final String CSV_HEADER = "output_folder,json_file,component_of_the_agent,action_applied_on_the_agent,description_of_the_action";

    public static String createReport(String outputFolderPath) {
        LocalDateTime timestamp = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");
        String timestampString = timestamp.format(formatter);

        String filename = "report_" + timestampString + ".csv";
        String filePath = outputFolderPath + "/" + filename;

        try (PrintWriter writer = new PrintWriter(new FileWriter(filePath))) {
            writer.println(CSV_HEADER);
        } catch (IOException e) {
            System.err.println("Error while creating the report CSV: " + e.getMessage());
        }

        return filePath;
    }

    public static void writeReportToCSV(String filePath, String componentName, String optionName, String description, String outputFolder, String jsonFile) {
        try (PrintWriter writer = new PrintWriter(new FileWriter(filePath, true))) {
            writer.println(outputFolder + "," + jsonFile + "," + componentName + "," + optionName + "," + description);
        } catch (IOException e) {
            System.err.println("Error while writing the report CSV: " + e.getMessage());
        }
    }
}
