package it.unimib.output;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import java.util.logging.*;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static it.unimib.utils.CommonUtilities.getEnvironmentVariable;

public class JsonWriter {
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static void writeJsonFile(Path jsonPath, JsonElement jsonElement, boolean isOutputEntireFolder, String option, String componentName,
                                     Path outputFolderPath) throws IOException {

        String fileName = jsonPath.getFileName().toString();

        // Path to the input folder, from which the chatbots to be mutated are read, via environment variable
        String inputFolderPath = getEnvironmentVariable("INPUT_FOLDER_PATH");

        String pathToFileName = Paths.get(inputFolderPath).relativize(jsonPath.toAbsolutePath()).toString();

        // Retrieving the output path to which to write the json file
        Path outputPath = getOutputPath(fileName, pathToFileName, isOutputEntireFolder, outputFolderPath);

        try (FileWriter writer = new FileWriter(outputPath.toFile())) {
            LogWriter.writeLog("Attempt to perform the '" + option + "' operation, from the component called '" + componentName + "'");
            gson.toJson(jsonElement, writer);
            LogWriter.writeLog("The JSON file called '" + fileName +"' has been edited and written correctly");
        } catch (IOException e) {
            LogWriter.writeErrorLog(Level.SEVERE, "Error writing " + fileName  + " : {}", e.getMessage());
        }

    }

    private static Path getOutputPath(String fileName, String pathToFileName, boolean isOutputEntireFolder, Path uniqueFolderPath){
        Path filePath;
        if(isOutputEntireFolder)
            filePath = Paths.get(pathToFileName);
        else
            filePath = Paths.get(fileName);

        return Paths.get(uniqueFolderPath.toString()).resolve(filePath);
    }
}