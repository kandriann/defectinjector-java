package it.unimib.modifications.chatbot;

import com.google.gson.JsonObject;
import it.unimib.output.JsonWriter;
import it.unimib.output.LogWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Consumer;
import java.util.logging.Level;

import static it.unimib.utils.CommonUtilities.getEnvironmentVariable;
import static it.unimib.utils.CommonUtilities.readJsonFile;

public class ChatbotModifier {
    Path jsonPath;
    String mutationName;
    String componentName;
    Path outputFolderPath;
    boolean isOutputEntireFolder;
    public ChatbotModifier(Path jsonPath, String mutationName, String componentName, Path outputFolderPath, boolean isOutputEntireFolder){
        this.jsonPath=jsonPath;
        this.mutationName=mutationName;
        this.componentName=componentName;
        this.outputFolderPath=outputFolderPath;
        this.isOutputEntireFolder=isOutputEntireFolder;
    }

    public void modifyChatbot() throws IOException {
        if (!(mutationName.equals("removeIntent") | mutationName.equals("removeEntity"))) {
            Map<String, Consumer<JsonObject>> mapChatbot = createMutationsMapChatbot();

            Consumer<JsonObject> operationChatbot = mapChatbot.get(mutationName);

            // Lettura del file json
            JsonObject jsonObject = readJsonFile(jsonPath);

            // Modifica del json
            operationChatbot.accept(jsonObject);

            try {
                JsonWriter.writeJsonFile(jsonPath, jsonObject, isOutputEntireFolder, mutationName, componentName, outputFolderPath);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        } else {
            String fileName = jsonPath.getFileName().toString();

            // Percorso della cartella di input (da cui si leggono i chatbot da mutare) tramite variabile d'ambiente
            String inputFolderPath = getEnvironmentVariable("INPUT_FOLDER_PATH");

            String pathToFileName = Paths.get(inputFolderPath).relativize(jsonPath.toAbsolutePath()).toString();

            // Recupero l'output path sul quale scrivere il file json
            Path outputPath = getOutputPath(fileName, pathToFileName, isOutputEntireFolder, outputFolderPath);

            try {
                LogWriter.writeLog("Attempt to perform the '" + mutationName + "' operation, from the component called '" + componentName + "'");
                Files.delete(outputPath);
                LogWriter.writeLog("The JSON file called '" + fileName +"' was successfully deleted");
            } catch (IOException e) {
                LogWriter.writeErrorLog(Level.SEVERE, "Error deleting " + fileName  + " : {}", e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public static Map<String, Consumer<JsonObject>> createMutationsMapChatbot() {
        Map<String, Consumer<JsonObject>> mutations = new HashMap<>();
        mutations.put("removeName", ChatbotOperations::remove_chatbot_name);
        mutations.put("changeName", ChatbotOperations::edit_chatbot_name);
        mutations.put("removeLanguage", ChatbotOperations::remove_chatbot_language);
        mutations.put("editLanguage", ChatbotOperations::edit_chatbot_language);
        mutations.put("editLanguages", ChatbotOperations::edit_chatbot_languages);
        mutations.put("removeLanguages", ChatbotOperations::remove_chatbot_languages);
        mutations.put("shuffleLanguages", ChatbotOperations::shuffle_chatbot_languages);
        return mutations;
    }

    private static Path getOutputPath(String fileName, String pathToFileName, boolean isOutputEntireFolder, Path uniqueFolderPath){
        Path filePath;
        if(isOutputEntireFolder)
            filePath = Paths.get(pathToFileName);
        else
            filePath = Paths.get(fileName);

        return Paths.get(uniqueFolderPath.toString()).resolve(filePath);
    }
}
