package it.unimib.modifications.chatbot;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static it.unimib.utils.CommonUtilities.logAttributeInfo;
import static it.unimib.utils.StringUtilities.getRandomStringGenerator;
import static it.unimib.utils.LanguageUtilities.getRandomLanguageGenerator;

public class ChatbotOperations {
    public static void remove_chatbot_name(JsonObject chatbot) {
        String attribute_name = "displayName";
        logAttributeInfo(chatbot, attribute_name);
        chatbot.remove(attribute_name);
    }

    public static void edit_chatbot_name(JsonObject chatbot) {
        String attribute_name = "displayName";
        logAttributeInfo(chatbot, attribute_name);
        chatbot.addProperty(attribute_name, getRandomStringGenerator().get());
    }

    public static void remove_chatbot_language(JsonObject chatbot) {
        String attribute_name = "language";
        logAttributeInfo(chatbot, attribute_name);
        chatbot.remove(attribute_name);
    }

    public static void edit_chatbot_language(JsonObject chatbot) {
        String attribute_name = "language";
        logAttributeInfo(chatbot, attribute_name);
        chatbot.addProperty(attribute_name, getRandomLanguageGenerator().get());
    }

    public static void edit_chatbot_languages(JsonObject chatbot) {
        String attribute_name = "supportedLanguages";
        logAttributeInfo(chatbot, attribute_name);
        JsonArray languagesArray = chatbot.getAsJsonArray(attribute_name);
        if (languagesArray != null && languagesArray.size() > 0) {
            for (int i = 0; i < languagesArray.size(); i++) {
                JsonElement languageElement = languagesArray.get(i);
                if (languageElement.isJsonPrimitive() && languageElement.getAsJsonPrimitive().isString()) {
                    // Modifica la stringa del linguaggio utilizzando getRandomLanguageGenerator()
                    String updatedLanguage = getRandomLanguageGenerator().get();
                    languagesArray.set(i, new JsonPrimitive(updatedLanguage));
                }
            }
        }
    }

    public static void remove_chatbot_languages(JsonObject chatbot) {
        String attribute_name = "supportedLanguages";
        logAttributeInfo(chatbot, attribute_name);
        chatbot.remove(attribute_name);
    }

    public static void shuffle_chatbot_languages(JsonObject chatbot) {
        String attribute_name = "supportedLanguages";
        logAttributeInfo(chatbot, attribute_name);
        JsonArray languagesArray = chatbot.getAsJsonArray(attribute_name);
        if (languagesArray != null && languagesArray.size() > 0) {
            List<String> languageList = new ArrayList<>();

            // Estrai i linguaggi dalla JsonArray e inseriscili in una lista
            for (JsonElement languageElement : languagesArray) {
                if (languageElement.isJsonPrimitive() && languageElement.getAsJsonPrimitive().isString()) {
                    languageList.add(languageElement.getAsJsonPrimitive().getAsString());
                }
            }

            // Mescola la lista di linguaggi
            Collections.shuffle(languageList);

            // Sostituisci gli elementi nella JsonArray con quelli mescolati
            for (int i = 0; i < languageList.size(); i++) {
                languagesArray.set(i, new JsonPrimitive(languageList.get(i)));
            }
        }
    }
}
