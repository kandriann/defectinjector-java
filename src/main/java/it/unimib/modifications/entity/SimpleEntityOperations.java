package it.unimib.modifications.entity;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import it.unimib.output.LogWriter;
import java.util.Random;
import static it.unimib.utils.StringUtilities.getRandomStringGenerator;

public class SimpleEntityOperations {
    public static void remove_simple_entity_name(JsonObject entity, JsonArray array) {
        entity.remove("name");
    }

    public static void edit_simple_entity_name(JsonObject entity, JsonArray array) {
        entity.addProperty("name", getRandomStringGenerator().get());
    }

    public static void edit_simple_input_value(JsonObject entity, JsonArray simpleInput) {
        if (simpleInput != null && simpleInput.size() > 0) {
            Random random = new Random();
            int randomIndex = random.nextInt(simpleInput.size());
            JsonObject selectedItem = simpleInput.get(randomIndex).getAsJsonObject();

            String oldValue = selectedItem.get("value").getAsString();
            String newValue = getRandomStringGenerator().get();

            selectedItem.addProperty("value", newValue);
            JsonArray synonyms = selectedItem.getAsJsonArray("synonyms");
            if (synonyms != null && synonyms.size() > 0) {
                for (int i = 0; i < synonyms.size(); i++) {
                    if (synonyms.get(i).getAsString().equals(oldValue)) {
                        synonyms.set(i, new JsonPrimitive(newValue));
                        LogWriter.writeLog("The string '" + oldValue + "' contained in 'value' and 'synonyms' has been replaced.");
                        break;
                    }
                }
            }
        }
    }
}
