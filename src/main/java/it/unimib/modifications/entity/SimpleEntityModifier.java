package it.unimib.modifications.entity;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.unimib.output.JsonWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import static it.unimib.utils.CommonUtilities.readJsonArray;
import static it.unimib.utils.CommonUtilities.readJsonFile;

public class SimpleEntityModifier {
    Path jsonPath;
    String mutationName;
    String componentName;
    Path outputFolderPath;
    boolean isOutputEntireFolder;
    public SimpleEntityModifier(Path jsonPath, String mutationName, String componentName, Path outputFolderPath, boolean isOutputEntireFolder){
        this.jsonPath=jsonPath;
        this.mutationName=mutationName;
        this.componentName=componentName;
        this.outputFolderPath=outputFolderPath;
        this.isOutputEntireFolder=isOutputEntireFolder;
    }

    public void modifySimpleEntity() throws IOException {
        Map<String, BiConsumer<JsonObject, JsonArray>> mapEntity = createOptionsMapSimpleEntity();
        BiConsumer<JsonObject, JsonArray> operationEntity = mapEntity.get(mutationName);

        JsonArray jsonArray = null;
        JsonObject jsonObject = null;

        if (isJsonObjectMutation()) {
            jsonObject = readJsonFile(jsonPath);
        } else {
            jsonArray = readJsonArray(jsonPath);
        }

        operationEntity.accept(jsonObject, jsonArray);

        // Converts the JsonArray to a JsonElement object so that it can be handled by the shared write method
        JsonElement jsonElementArray = jsonArray;
        JsonElement jsonElementObject = jsonObject;

        try {
            if (isJsonObjectMutation()) {
                JsonWriter.writeJsonFile(jsonPath, jsonElementObject, isOutputEntireFolder, mutationName, componentName, outputFolderPath);
            } else {
                JsonWriter.writeJsonFile(jsonPath, jsonElementArray, isOutputEntireFolder, mutationName, componentName, outputFolderPath);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, BiConsumer<JsonObject, JsonArray>> createOptionsMapSimpleEntity() {
        Map<String, BiConsumer<JsonObject, JsonArray>> mutations = new HashMap<>();
        mutations.put("removeName", SimpleEntityOperations::remove_simple_entity_name);
        mutations.put("changeName", SimpleEntityOperations::edit_simple_entity_name);
        mutations.put("changeEntityValue", SimpleEntityOperations::edit_simple_input_value);
        return mutations;
    }

    public boolean isJsonObjectMutation() {
        return "changeName".equals(mutationName) || "removeName".equals(mutationName);
    }
}
