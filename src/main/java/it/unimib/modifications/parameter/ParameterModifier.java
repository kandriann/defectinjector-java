package it.unimib.modifications.parameter;

import com.google.gson.JsonObject;
import it.unimib.output.JsonWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static it.unimib.utils.CommonUtilities.readJsonFile;

public class ParameterModifier {
    Path jsonPath;
    String mutationName;
    String componentName;
    Path outputFolderPath;
    boolean isOutputEntireFolder;

    public ParameterModifier(Path jsonPath, String mutationName, String componentName, Path outputFolderPath, boolean isOutputEntireFolder){
        this.jsonPath=jsonPath;
        this.mutationName=mutationName;
        this.componentName=componentName;
        this.outputFolderPath=outputFolderPath;
        this.isOutputEntireFolder=isOutputEntireFolder;
    }

    public void modifyParameters() throws IOException {
        Map<String, Consumer<JsonObject>> mapParameter = createMutationsMapParameter();
        Consumer<JsonObject> operationParameter = mapParameter.get(mutationName);

        JsonObject jsonObject = readJsonFile(jsonPath);
        operationParameter.accept(jsonObject);

        try {
            JsonWriter.writeJsonFile(jsonPath, jsonObject, isOutputEntireFolder, mutationName, componentName, outputFolderPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, Consumer<JsonObject>> createMutationsMapParameter() {
        Map<String, Consumer<JsonObject>> mutations = new HashMap<>();

        mutations.put("editName", ParameterOperations::modify_parameters_name_random);
        mutations.put("removeName", ParameterOperations::modify_parameters_remove_name);
        mutations.put("toggleRequired", ParameterOperations::modify_parameters_required);
        mutations.put("removeRequired", ParameterOperations::modify_parameters_required_null);
        mutations.put("toggleList", ParameterOperations::modify_parameters_isList);
        mutations.put("removeList", ParameterOperations::modify_parameters_isList_null);
        mutations.put("removePrompt", ParameterOperations::modify_parameters_prompts);
        mutations.put("removePrompts", ParameterOperations::modify_parameters_prompts_null);

        return mutations;
    }
}
