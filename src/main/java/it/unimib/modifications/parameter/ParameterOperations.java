package it.unimib.modifications.parameter;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.unimib.output.LogWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static it.unimib.utils.StringUtilities.getRandomStringGenerator;

public class ParameterOperations {

    public static void modify_parameters_remove_name(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray parameters = response.getAsJsonArray("parameters");
                if (parameters != null && parameters.size() > 0) {
                    List<String> selectedIds = selectParametersToModify(parameters);
                    for (JsonElement parameterElement : parameters) {
                        JsonObject parameter = parameterElement.getAsJsonObject();
                        String parameterId = parameter.get("id").getAsString();
                        if (selectedIds.contains(parameterId)) {
                            if (parameter.has("name")) {
                                LogWriter.writeLog("The 'name' attribute of the parameter with id " + parameterId + " will be removed");
                                parameter.remove("name");
                            } else {
                                LogWriter.writeLog("The indicated json file does not have the attribute 'name' to be mutated.");
                            }
                        }
                    }
                }
            }
        }
    }

    public static void modify_parameters_name_random(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray parameters = response.getAsJsonArray("parameters");
                if (parameters != null && parameters.size() > 0) {
                    List<String> selectedIds = selectParametersToModify(parameters);
                    for (JsonElement parameterElement : parameters) {
                        JsonObject parameter = parameterElement.getAsJsonObject();
                        String parameterId = parameter.get("id").getAsString();
                        if (selectedIds.contains(parameterId)) {
                            LogWriter.writeLog("The 'name' attribute of the parameter with id " + parameterId + " will be changed");
                            parameter.addProperty("name", getRandomStringGenerator().get());
                        }
                    }
                }
            }
        }
    }

    public static void modify_parameters_isList(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray parameters = response.getAsJsonArray("parameters");
                if (parameters != null && parameters.size() > 0) {
                    List<String> selectedIds = selectParametersToModify(parameters);
                    for (JsonElement parameterElement : parameters) {
                        JsonObject parameter = parameterElement.getAsJsonObject();
                        String parameterId = parameter.get("id").getAsString();
                        if (selectedIds.contains(parameterId)) {
                            JsonElement isListElement = parameter.get("isList");
                            if (isListElement != null && isListElement.isJsonPrimitive() && isListElement.getAsJsonPrimitive().isBoolean()) {
                                boolean isList = isListElement.getAsBoolean();
                                LogWriter.writeLog("The 'isList' attribute of the parameter with id " + parameterId + " will be changed");
                                parameter.addProperty("isList", !isList);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void modify_parameters_isList_null(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray parameters = response.getAsJsonArray("parameters");
                if (parameters != null && parameters.size() > 0) {
                    List<String> selectedIds = selectParametersToModify(parameters);
                    for (JsonElement parameterElement : parameters) {
                        JsonObject parameter = parameterElement.getAsJsonObject();
                        String parameterId = parameter.get("id").getAsString();
                        if (selectedIds.contains(parameterId)) {
                            if (parameter.has("isList")) {
                                LogWriter.writeLog("The 'isList' attribute of the parameter with id " + parameterId + " will be removed");
                                parameter.remove("isList");
                            } else {
                                LogWriter.writeLog("The indicated json file does not have the attribute 'isList' to be mutated.");
                            }
                        }
                    }
                }
            }
        }
    }

    public static void modify_parameters_required(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray parameters = response.getAsJsonArray("parameters");
                if (parameters != null && parameters.size() > 0) {
                    List<String> selectedIds = selectParametersToModify(parameters);
                    for (JsonElement parameterElement : parameters) {
                        JsonObject parameter = parameterElement.getAsJsonObject();
                        String parameterId = parameter.get("id").getAsString();
                        if (selectedIds.contains(parameterId)) {
                            JsonElement requiredElement = parameter.get("required");
                            if (requiredElement != null && requiredElement.isJsonPrimitive() && requiredElement.getAsJsonPrimitive().isBoolean()) {
                                boolean required = requiredElement.getAsBoolean();
                                LogWriter.writeLog("The 'required' attribute of the parameter with id " + parameterId + " will be changed");
                                parameter.addProperty("required", !required);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void modify_parameters_required_null(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray parameters = response.getAsJsonArray("parameters");
                if (parameters != null && parameters.size() > 0) {
                    List<String> selectedIds = selectParametersToModify(parameters);
                    for (JsonElement parameterElement : parameters) {
                        JsonObject parameter = parameterElement.getAsJsonObject();
                        String parameterId = parameter.get("id").getAsString();
                        if (selectedIds.contains(parameterId)) {
                            if (parameter.has("required")) {
                                LogWriter.writeLog("The 'required' attribute of the parameter with id " + parameterId + " will be removed");
                                parameter.remove("required");
                            } else {
                                LogWriter.writeLog("The indicated json file does not have the attribute 'required' to be mutated.");
                            }
                        }
                    }
                }
            }
        }
    }

    public static void modify_parameters_prompts(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray parameters = response.getAsJsonArray("parameters");
                if (parameters != null && parameters.size() > 0) {
                    List<String> selectedIds = selectParametersToModify(parameters);
                    for (JsonElement parameterElement : parameters) {
                        JsonObject parameter = parameterElement.getAsJsonObject();
                        String parameterId = parameter.get("id").getAsString();
                        if (selectedIds.contains(parameterId)) {
                            JsonElement promptsElement = parameter.get("prompts");
                            if (promptsElement != null && promptsElement.isJsonArray()) {
                                JsonArray prompts = promptsElement.getAsJsonArray();
                                if (prompts.size() > 0) {
                                    LogWriter.writeLog("One of the objects 'prompt' of the parameter with id " + parameterId + " will be removed");
                                    int randomIndex = new Random().nextInt(prompts.size());
                                    prompts.remove(randomIndex);
                                } else{
                                    LogWriter.writeLog("The 'prompts' attribute of the parameter with id " + parameterId + " has no prompt objects within it");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void modify_parameters_prompts_null(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray parameters = response.getAsJsonArray("parameters");
                if (parameters != null && parameters.size() > 0) {
                    List<String> selectedIds = selectParametersToModify(parameters);
                    for (JsonElement parameterElement : parameters) {
                        JsonObject parameter = parameterElement.getAsJsonObject();
                        String parameterId = parameter.get("id").getAsString();
                        if (selectedIds.contains(parameterId)) {
                            if (parameter.has("prompts")) {
                                LogWriter.writeLog("The 'prompts' attribute of the parameter with id " + parameterId + " will be removed");
                                parameter.remove("prompts");
                            } else {
                                LogWriter.writeLog("The indicated json file does not have the attribute 'prompts' to be mutated.");
                            }
                        }
                    }
                }
            }
        }
    }

    public static List<String> selectParametersToModify(JsonArray parameters) {
        List<String> selectedIds = new ArrayList<>();
        Random random = new Random();
        int numParameters = parameters.size();

        List<String> availableIds = new ArrayList<>();
        for (int i = 0; i < numParameters; i++) {
            JsonObject parameter = parameters.get(i).getAsJsonObject();
            String parameterId = parameter.get("id").getAsString();
            availableIds.add(parameterId);
        }

        int numToModify = random.nextInt(availableIds.size()) + 1;
        LogWriter.writeLog("The number of parameters that will be changed is: " + numToModify);

        for (int i = 0; i < numToModify; i++) {
            int randomIndex = random.nextInt(availableIds.size());
            String selectedId = availableIds.get(randomIndex);
            availableIds.remove(randomIndex);
            selectedIds.add(selectedId);
        }

        return selectedIds;
    }
}
