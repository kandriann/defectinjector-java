package it.unimib.modifications.intent;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.unimib.output.LogWriter;
import it.unimib.utils.IntegerUtilities;

import java.util.Random;
import java.util.function.Supplier;

import static it.unimib.utils.StringUtilities.getRandomStringGenerator;

public class IntentOperations {
    private static final Random random = new Random();
    public static void remove_intent_name(JsonObject intent) {
        intent.remove("name");
    }

    public static void edit_intent_name(JsonObject intent) {
        intent.addProperty("name", getRandomStringGenerator().get());
    }

    public static void remove_intent_priority(JsonObject intent) {
        intent.remove("priority");
    }

    public static void edit_intent_priority(JsonObject intent) {
        int currentPriority = 0; // is the minimum value that can be associated
        JsonElement priorityElement = intent.get("priority");
        if (priorityElement != null && priorityElement.isJsonPrimitive() && priorityElement.getAsJsonPrimitive().isNumber()) {
            currentPriority = priorityElement.getAsInt();
            LogWriter.writeLog("The priority attribute is present and it is: " + currentPriority);
        } else {
            LogWriter.writeLog("The priority attribute is not present, it will be assigned the value 0, as the starting value");
        }
        Supplier<Integer> randomGenerator = IntegerUtilities.getRandomIntegerGenerator(currentPriority, "priority");
        int newPriority = randomGenerator.get();
        intent.addProperty("priority", newPriority);
    }

    public static void remove_intent_fallback(JsonObject intent) {
        intent.addProperty("fallbackIntent", "");
        //intent.remove("fallbackIntent");
    }
    public static void toggle_fallback_intent(JsonObject intent) {
        boolean fallback = intent.get("fallbackIntent").getAsBoolean();
        intent.addProperty("fallbackIntent", !fallback);
    }

    public static void remove_random_parameter(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray parameters = response.getAsJsonArray("parameters");
                if (parameters != null && parameters.size() > 0) {
                    int indexToRemove = random.nextInt(parameters.size());
                    parameters.remove(indexToRemove);
                }
            }
        }
    }

    public static void remove_all_parameters(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                response.remove("parameters");
            }
        }
    }

    public static void remove_random_input_context(JsonObject intent) {
        JsonArray contexts = intent.getAsJsonArray("contexts");
        if (contexts != null && contexts.size() > 0) {
            int indexToRemove = random.nextInt(contexts.size());
            contexts.remove(indexToRemove);
        }
    }

    public static void remove_all_input_contexts(JsonObject intent) {
        intent.remove("contexts");
    }

    public static void remove_random_output_context(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray affectedContexts = response.getAsJsonArray("affectedContexts");
                if (affectedContexts != null && affectedContexts.size() > 0) {
                    int indexToRemove = random.nextInt(affectedContexts.size());
                    affectedContexts.remove(indexToRemove);
                }
            }
        }
    }

    public static void remove_all_output_contexts(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                response.remove("affectedContexts");
            }
        }
    }
}
