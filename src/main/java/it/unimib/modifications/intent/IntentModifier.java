package it.unimib.modifications.intent;

import com.google.gson.JsonObject;
import it.unimib.output.JsonWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static it.unimib.utils.CommonUtilities.readJsonFile;

public class IntentModifier {
    Path jsonPath;
    String mutationName;
    String componentName;
    Path outputFolderPath;
    boolean isOutputEntireFolder;
    public IntentModifier(Path jsonPath, String mutationName, String componentName, Path outputFolderPath, boolean isOutputEntireFolder){
        this.jsonPath=jsonPath;
        this.mutationName=mutationName;
        this.componentName=componentName;
        this.outputFolderPath=outputFolderPath;
        this.isOutputEntireFolder=isOutputEntireFolder;
    }

    public void modifyIntents() throws IOException {
        Map<String, Consumer<JsonObject>> mapIntent = createMutationsMapIntent();
        Consumer<JsonObject> operationIntent = mapIntent.get(mutationName);

        JsonObject jsonObject = readJsonFile(jsonPath);
        operationIntent.accept(jsonObject);

        try {
            JsonWriter.writeJsonFile(jsonPath, jsonObject, isOutputEntireFolder, mutationName, componentName, outputFolderPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, Consumer<JsonObject>> createMutationsMapIntent() {
        Map<String, Consumer<JsonObject>> mutations = new HashMap<>();

        mutations.put("removeName", IntentOperations::remove_intent_name);
        mutations.put("editName", IntentOperations::edit_intent_name);
        mutations.put("removePriority", IntentOperations::remove_intent_priority);
        mutations.put("editPriority", IntentOperations::edit_intent_priority);
        mutations.put("removeFallback", IntentOperations::remove_intent_fallback);
        mutations.put("toggleFallback", IntentOperations::toggle_fallback_intent);
        mutations.put("removeParameter", IntentOperations::remove_random_parameter);
        mutations.put("removeParameters", IntentOperations::remove_all_parameters);
        mutations.put("removeRandomInputContext", IntentOperations::remove_random_input_context);
        mutations.put("removeAllInputContexts", IntentOperations::remove_all_input_contexts);
        mutations.put("removeRandomOutputContext", IntentOperations::remove_random_output_context);
        mutations.put("removeAllOutputContexts", IntentOperations::remove_all_output_contexts);

        return mutations;
    }
}
