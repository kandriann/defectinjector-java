package it.unimib.modifications.text_action;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import it.unimib.output.LogWriter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static it.unimib.utils.StringUtilities.getRandomStringGenerator;

public class TextActionOperations {

    public static void edit_text_action(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray messages = response.getAsJsonArray("messages");

                if (messages != null && messages.size() > 0) {
                    // Rule used to identify and distinguish TextAction
                    List<JsonObject> candidates = new ArrayList<>();
                    for (JsonElement messageElement : messages) {
                        JsonObject message = messageElement.getAsJsonObject();
                        if (message.has("type") && message.get("type").getAsString().equals("0") &&
                                message.has("textToSpeech")) {
                            candidates.add(message);
                        }
                    }

                    if (!candidates.isEmpty()) {
                        Random random = new Random();
                        JsonObject selectedMessage = candidates.get(random.nextInt(candidates.size()));
                        JsonArray speech = selectedMessage.getAsJsonArray("speech");

                        if (speech != null && speech.size() > 0) {
                            int randomIndex = random.nextInt(speech.size());

                            String oldSpeech = String.valueOf(speech.get(randomIndex));
                            String newSpeech = getRandomStringGenerator().get();
                            speech.set(randomIndex, new JsonPrimitive(newSpeech));

                            LogWriter.writeLog("The string '" + oldSpeech + "' contained in 'speech' has been replaced.");
                        } else {
                            LogWriter.writeLog("The indicated json file either does not have the 'speech' attribute or has it empty.");
                        }
                    } else {
                        LogWriter.writeLog("Within the indicated json file, there are no TextAction.");
                    }
                }
            }
        }
    }
}
