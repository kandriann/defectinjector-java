package it.unimib.modifications.text_action;

import com.google.gson.JsonObject;
import it.unimib.modifications.entity.SimpleEntityOperations;
import it.unimib.output.JsonWriter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static it.unimib.utils.CommonUtilities.readJsonFile;

public class TextActionModifier {
    Path jsonPath;
    String mutationName;
    String componentName;
    Path outputFolderPath;
    boolean isOutputEntireFolder;
    public TextActionModifier(Path jsonPath, String mutationName, String componentName, Path outputFolderPath, boolean isOutputEntireFolder){
        this.jsonPath=jsonPath;
        this.mutationName=mutationName;
        this.componentName=componentName;
        this.outputFolderPath=outputFolderPath;
        this.isOutputEntireFolder=isOutputEntireFolder;
    }

    public void modifyTextAction() throws IOException {
        Map<String, Consumer<JsonObject>> mapTextAction = createOptionsMapTextAction();
        Consumer<JsonObject> operationTextAction = mapTextAction.get(mutationName);

        JsonObject jsonObject = readJsonFile(jsonPath);
        operationTextAction.accept(jsonObject);

        try {
            JsonWriter.writeJsonFile(jsonPath, jsonObject, isOutputEntireFolder, mutationName, componentName, outputFolderPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, Consumer<JsonObject>> createOptionsMapTextAction() {
        Map<String, Consumer<JsonObject>> mutations = new HashMap<>();
        mutations.put("editTextAction", TextActionOperations::edit_text_action);
        return mutations;
    }
}
