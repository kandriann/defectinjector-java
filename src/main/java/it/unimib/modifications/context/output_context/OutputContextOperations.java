package it.unimib.modifications.context.output_context;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.unimib.output.LogWriter;
import it.unimib.utils.IntegerUtilities;
import java.util.*;
import java.util.function.Supplier;

import static it.unimib.utils.StringUtilities.getRandomStringGenerator;

public class OutputContextOperations {
    public static void remove_output_context_name(JsonObject context) {
        JsonArray responses = context.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray affectedContexts = response.getAsJsonArray("affectedContexts");
                if (affectedContexts != null && affectedContexts.size() > 0) {
                    // Genera un numero casuale tra 0 e il numero di contesti - 1
                    int randomIndex = new Random().nextInt(affectedContexts.size());

                    JsonElement affectedContext = affectedContexts.get(randomIndex);
                    if (affectedContext.isJsonObject()) {
                        JsonObject contextObject = affectedContext.getAsJsonObject();
                        // Controlla se l'oggetto contextObject ha un attributo "name" e rimuovilo
                        if (contextObject.has("name")) {
                            contextObject.remove("name");
                        } else {
                            LogWriter.writeLog("The attribute 'name' is not present");
                        }
                    }
                } else {
                    // Logga il fatto che "affectedContexts" è vuoto
                    LogWriter.writeLog("The attribute 'affectedContexts' is empty");
                }
            }
        }
    }

    public static void edit_output_context_name(JsonObject context) {
        JsonArray responses = context.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray affectedContexts = response.getAsJsonArray("affectedContexts");
                if (affectedContexts != null && affectedContexts.size() > 0) {
                    // Genera un numero casuale tra 0 e il numero di contesti - 1
                    int randomIndex = new Random().nextInt(affectedContexts.size());

                    JsonElement affectedContext = affectedContexts.get(randomIndex);
                    if (affectedContext.isJsonObject()) {
                        JsonObject contextObject = affectedContext.getAsJsonObject();
                        // Modifica l'attributo "name" con una stringa casuale
                        contextObject.addProperty("name", getRandomStringGenerator().get());
                    }
                } else {
                    // Logga il fatto che "affectedContexts" è vuoto
                    LogWriter.writeLog("The attribute 'affectedContexts' is empty");
                }
            }
        }
    }

    public static void remove_output_context_lifespan(JsonObject context) {
        JsonArray responses = context.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray affectedContexts = response.getAsJsonArray("affectedContexts");
                if (affectedContexts != null && affectedContexts.size() > 0) {
                    // Genera un numero casuale tra 0 e il numero di contesti - 1
                    int randomIndex = new Random().nextInt(affectedContexts.size());

                    JsonElement affectedContext = affectedContexts.get(randomIndex);
                    if (affectedContext.isJsonObject()) {
                        JsonObject contextObject = affectedContext.getAsJsonObject();
                        // Controlla se l'oggetto contextObject ha un attributo "name" e rimuovilo
                        if (contextObject.has("lifespan")) {
                            contextObject.remove("lifespan");
                        } else {
                            LogWriter.writeLog("The attribute 'lifespan' is not present");
                        }
                    }
                } else {
                    // Logga il fatto che "affectedContexts" è vuoto
                    LogWriter.writeLog("The attribute 'affectedContexts' is empty");
                }
            }
        }
    }

    public static void edit_output_context_lifespan(JsonObject context) {
        int currentLifespan = 1; // is the minimum value that can be associated

        JsonArray responses = context.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray affectedContexts = response.getAsJsonArray("affectedContexts");
                if (affectedContexts != null && affectedContexts.size() > 0) {
                    // Genera un numero casuale tra 0 e il numero di contesti - 1
                    int randomIndex = new Random().nextInt(affectedContexts.size());

                    JsonElement affectedContext = affectedContexts.get(randomIndex);
                    if (affectedContext.isJsonObject()) {
                        JsonObject contextObject = affectedContext.getAsJsonObject();

                        // Modifica l'attributo "lifespan"
                        JsonElement lifespanElement = contextObject.get("lifespan");
                        if (lifespanElement != null && lifespanElement.isJsonPrimitive() && lifespanElement.getAsJsonPrimitive().isNumber()) {
                            currentLifespan = lifespanElement.getAsInt();
                            LogWriter.writeLog("The lifespanCount attribute is present and it is: " + currentLifespan);
                        } else {
                            LogWriter.writeLog("The lifespanCount attribute is not present, it will be assigned the value 0, as the starting value");
                        }
                        Supplier<Integer> randomGenerator = IntegerUtilities.getRandomIntegerGenerator(currentLifespan, "lifespan");
                        int newLifespan = randomGenerator.get();
                        contextObject.addProperty("lifespan", newLifespan);

                    }
                } else {
                    // Logga il fatto che "affectedContexts" è vuoto
                    LogWriter.writeLog("The attribute 'affectedContexts' is empty");
                }
            }
        }
    }

    public static void remove_random_parameter_from_output_context(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray affectedContexts = response.getAsJsonArray("affectedContexts");
                if (affectedContexts != null && affectedContexts.size() > 0) {
                    // Scegli casualmente un contesto tra quelli in affectedContexts
                    int randomIndex = new Random().nextInt(affectedContexts.size());
                    JsonElement affectedContext = affectedContexts.get(randomIndex);

                    if (affectedContext.isJsonObject()) {
                        JsonObject contextObject = affectedContext.getAsJsonObject();
                        JsonElement parametersElement = contextObject.get("parameters");

                        if (parametersElement != null && parametersElement.isJsonObject()) {
                            JsonObject parameters = parametersElement.getAsJsonObject();

                            if (parameters.size() > 0) {
                                // Ottieni tutte le chiavi di "parameters"
                                Set<String> parameterKeys = parameters.keySet();
                                // Trasforma l'insieme in un array
                                String[] keysArray = parameterKeys.toArray(new String[0]);
                                // Scegli casualmente un indice da rimuovere
                                int randomKeyIndex = new Random().nextInt(keysArray.length);
                                // Ottieni la chiave selezionata
                                String keyToRemove = keysArray[randomKeyIndex];
                                // Rimuovi la chiave e il suo valore da "parameters"
                                parameters.remove(keyToRemove);
                            }
                        } else {
                            // Logga se "parameters" è null o non è un oggetto
                            LogWriter.writeLog("The attribute 'parameters' is null or not an object");
                        }
                    }
                } else {
                    // Logga il fatto che "affectedContexts" è vuoto
                    LogWriter.writeLog("The attribute 'affectedContexts' is empty");
                }
            }
        }
    }

    public static void remove_all_parameters_from_output_context(JsonObject intent) {
        JsonArray responses = intent.getAsJsonArray("responses");
        if (responses != null && responses.size() > 0) {
            for (JsonElement responseElement : responses) {
                JsonObject response = responseElement.getAsJsonObject();
                JsonArray affectedContexts = response.getAsJsonArray("affectedContexts");
                if (affectedContexts != null && affectedContexts.size() > 0) {
                    // Scegli casualmente un contesto tra quelli in affectedContexts
                    int randomIndex = new Random().nextInt(affectedContexts.size());
                    JsonElement affectedContext = affectedContexts.get(randomIndex);

                    if (affectedContext.isJsonObject()) {
                        JsonObject contextObject = affectedContext.getAsJsonObject();
                        JsonElement parametersElement = contextObject.get("parameters");
                        if (parametersElement != null && parametersElement.isJsonObject()) {
                            contextObject.remove("parameters");
                        } else {
                            LogWriter.writeLog("The attribute 'parameters' is null or not an object");
                        }
                    }
                } else {
                    // Logga il fatto che "affectedContexts" è vuoto
                    LogWriter.writeLog("The attribute 'affectedContexts' is empty");
                }
            }
        }
    }
}
