package it.unimib.modifications.context.output_context;

import com.google.gson.JsonObject;
import it.unimib.output.JsonWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static it.unimib.utils.CommonUtilities.readJsonFile;

public class OutputContextModifier {
    Path jsonPath;
    String mutationName;
    String componentName;
    Path outputFolderPath;
    boolean isOutputEntireFolder;
    public OutputContextModifier(Path jsonPath, String mutationName, String componentName, Path outputFolderPath, boolean isOutputEntireFolder){
        this.jsonPath=jsonPath;
        this.mutationName=mutationName;
        this.componentName=componentName;
        this.outputFolderPath=outputFolderPath;
        this.isOutputEntireFolder=isOutputEntireFolder;
    }

    public void modifyContexts() throws IOException {
        Map<String, Consumer<JsonObject>> mapContext = createMutationsMapContext();
        Consumer<JsonObject> operationContext = mapContext.get(mutationName);

        JsonObject jsonObject = readJsonFile(jsonPath);
        operationContext.accept(jsonObject);

        try {
            JsonWriter.writeJsonFile(jsonPath, jsonObject, isOutputEntireFolder, mutationName, componentName, outputFolderPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, Consumer<JsonObject>> createMutationsMapContext() {
        Map<String, Consumer<JsonObject>> mutations = new HashMap<>();
        mutations.put("removeName", OutputContextOperations::remove_output_context_name);
        mutations.put("changeName", OutputContextOperations::edit_output_context_name);
        mutations.put("removeLifespan", OutputContextOperations::remove_output_context_lifespan);
        mutations.put("changeLifespan", OutputContextOperations::edit_output_context_lifespan);
        mutations.put("removeParameter", OutputContextOperations::remove_random_parameter_from_output_context);
        mutations.put("removeParameters", OutputContextOperations::remove_all_parameters_from_output_context);
        return mutations;
    }
}
