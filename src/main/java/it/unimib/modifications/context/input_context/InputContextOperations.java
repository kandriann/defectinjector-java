package it.unimib.modifications.context.input_context;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import it.unimib.output.LogWriter;
import java.util.Random;

import static it.unimib.utils.StringUtilities.getRandomStringGenerator;

public class InputContextOperations {

    private static final Random random = new Random();
    private static final Gson gson = new Gson();

    public static void remove_input_context_name(JsonObject context) {
        JsonArray contextsArray = context.getAsJsonArray("contexts");

        // Verifica se "contexts" è rappresentato da una stringa vuota
        if (contextsArray == null || contextsArray.size() == 0) {
            // Scrivi nel file di log
            LogWriter.writeLog("Contexts attribute is empty, no contexts to remove.");

        } else if (contextsArray.size() == 1) {
            // Se c'è un solo elemento, rimuovilo completamente
            contextsArray.remove(0);

        } else {
            // Se ci sono più elementi, rimuovi uno casualmente
            int randomIndex = random.nextInt(contextsArray.size());
            contextsArray.remove(randomIndex);
        }
    }


    public static void edit_input_context_name(JsonObject context) {
        // Trova l'array "contexts" nell'oggetto JSON
        JsonArray contextsArray = context.getAsJsonArray("contexts");

        // Verifica se "contexts" è rappresentato da una stringa vuota o è nullo
        if (contextsArray == null || contextsArray.size() == 0) {
            // Scrivi nel file di log
            LogWriter.writeLog("Contexts attribute is empty, no contexts to edit.");
        } else {
            // Scegli casualmente un indice nell'array
            int randomIndex = random.nextInt(contextsArray.size());

            // Genera una nuova stringa casuale
            String newContextName = getRandomStringGenerator().get();

            // Sostituisci la stringa esistente con la nuova
            JsonElement newContextElement = gson.toJsonTree(newContextName);
            contextsArray.set(randomIndex, newContextElement);
        }
    }
}
