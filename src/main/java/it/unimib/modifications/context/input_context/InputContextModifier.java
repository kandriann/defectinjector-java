package it.unimib.modifications.context.input_context;

import com.google.gson.JsonObject;
import it.unimib.output.JsonWriter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static it.unimib.utils.CommonUtilities.readJsonFile;

public class InputContextModifier {
    Path jsonPath;
    String mutationName;
    String componentName;
    Path outputFolderPath;
    boolean isOutputEntireFolder;
    public InputContextModifier(Path jsonPath, String mutationName, String componentName, Path outputFolderPath, boolean isOutputEntireFolder){
        this.jsonPath=jsonPath;
        this.mutationName=mutationName;
        this.componentName=componentName;
        this.outputFolderPath=outputFolderPath;
        this.isOutputEntireFolder=isOutputEntireFolder;
    }

    public void modifyContexts() throws IOException {
        Map<String, Consumer<JsonObject>> mapContext = createMutationsMapContext();
        Consumer<JsonObject> operationContext = mapContext.get(mutationName);

        JsonObject jsonObject = readJsonFile(jsonPath);
        operationContext.accept(jsonObject);

        try {
            JsonWriter.writeJsonFile(jsonPath, jsonObject, isOutputEntireFolder, mutationName, componentName, outputFolderPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, Consumer<JsonObject>> createMutationsMapContext() {
        Map<String, Consumer<JsonObject>> mutations = new HashMap<>();
        mutations.put("removeName", InputContextOperations::remove_input_context_name);
        mutations.put("changeName", InputContextOperations::edit_input_context_name);
        return mutations;
    }
}
