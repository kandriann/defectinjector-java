package it.unimib.input;

import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class XmlController {
    public static void validateConfigXml(String filePath) {
        try {
            DOMParser parser = new DOMParser();
            parser.setFeature("http://apache.org/xml/features/xinclude", true);
            parser.parse(filePath);

            Document doc = parser.getDocument();
            doc.getDocumentElement().normalize();

            // Check the value of the <root_folder> tag
            NodeList rootFolderList = doc.getElementsByTagName("root_folder");
            if (rootFolderList.getLength() == 0 | rootFolderList.getLength() > 1) {
                System.out.println("Error: config.xml must contain one <root_folder> tag.");
                System.exit(1); // Terminate the program
            }

            Node rootFolderNode = rootFolderList.item(0);
            String rootFolderValue = rootFolderNode.getTextContent().trim();
            if (rootFolderValue.isEmpty()) {
                System.out.println("Error: The <root_folder> tag cannot be empty.");
                System.exit(1); // Terminate the program
            } else {
                // Check if the path exists in the file system
                Path rootFolderPath = Paths.get(rootFolderValue);
                if (!Files.exists(rootFolderPath)) {
                    System.out.println("Error: The specified root folder path does not exist.");
                    System.exit(1); // Terminate the program
                }
            }

            // Check if config.xml contains a single <component> tag
            NodeList componentList = doc.getElementsByTagName("component");
            if (componentList.getLength() == 0) {
                System.out.println("Error: config.xml must contain at least one <component> tag.");
                System.exit(1); // Terminate the program
            }

            // Iterate through each <component> tag
            for (int i = 0; i < componentList.getLength(); i++) {
                Element componentElement = (Element) componentList.item(i);

                // Check if the <mutation> tags exist
                NodeList mutationList = componentElement.getElementsByTagName("mutation");
                for (int j = 0; j < mutationList.getLength(); j++) {
                    Element mutationElement = (Element) mutationList.item(j);

                    // Check if there is exactly one <enabled> tag
                    NodeList enabledList = mutationElement.getElementsByTagName("enabled");
                    if (enabledList.getLength() != 1) {
                        System.err.println("Error: Each <mutation> tag must contain exactly one <enabled> tag.");
                        System.exit(1); // Terminate the program
                    }
                    // Check the content of <enabled> tag
                    Element enabledElement = (Element) enabledList.item(0);
                    String enabledValue = enabledElement.getTextContent();
                    if (enabledValue == null || (!enabledValue.equalsIgnoreCase("true") && !enabledValue.equalsIgnoreCase("false"))) {
                        System.err.println("Error: The content inside <enabled> tag must be 'true' or 'false'.");
                        System.exit(1); // Terminate the program
                    }

                    // Check if the <file_path> is set when the tag <enabled> is set to "true"
                    if (enabledValue.equalsIgnoreCase("true")) {
                        NodeList filePathList = mutationElement.getElementsByTagName("file_path");
                        if (filePathList.getLength() < 1) {
                            System.err.println("Error: When <enabled> is set to 'true', at least one <file_path> tag must be provided.");
                            System.exit(1); // Terminate the program
                        } else {
                            // Check if <output_entire_folder> is provided when <file_path> is provided
                            NodeList outputFolderList = mutationElement.getElementsByTagName("output_entire_folder");
                            if (outputFolderList.getLength() < 1) {
                                System.err.println("Error: When <enabled> is set to 'true' and <file_path> is provided, "
                                        + "<output_entire_folder> must be provided with a value of 'true' or 'false'.");
                                System.exit(1); // Terminate the program
                            } else {
                                Element outputFolderElement = (Element) outputFolderList.item(0);
                                String outputFolderValue = outputFolderElement.getTextContent();
                                if (!outputFolderValue.equalsIgnoreCase("true") && !outputFolderValue.equalsIgnoreCase("false")) {
                                    System.err.println("Error: The content inside <output_entire_folder> tag must be 'true' or 'false'.");
                                    System.exit(1); // Terminate the program
                                }
                            }
                        }
                    }

                    // Check if <file_path> is a subpath of <root_folder>
                    //if (enabledValue.equalsIgnoreCase("true")) {
                    String rootFolderPath = doc.getElementsByTagName("root_folder").item(0).getTextContent();
                    NodeList filePathList = mutationElement.getElementsByTagName("file_path");

                    // Get the associated component name
                    String componentName = componentElement.getElementsByTagName("name").item(0).getTextContent();

                    for (int k = 0; k < filePathList.getLength(); k++) {
                        Element filePathElement = (Element) filePathList.item(k);
                        String filePathText = filePathElement.getTextContent();
                        String mutationName = mutationElement.getElementsByTagName("name").item(0).getTextContent();
                        if(filePathText != null && !filePathText.isEmpty()) {
                            if (!filePathText.startsWith(rootFolderPath)) {
                                System.err.println("Error: The path in <file_path> must be a subpath of the <root_folder> path.");
                                System.exit(1); // Terminate the program
                            }

                            if (componentName.equalsIgnoreCase("intent")) {
                                if (!isIntentFile(filePathText)) {
                                    System.err.println("Error: Invalid file specified in <file_path>: " + filePathText);
                                    System.exit(1); // Terminate the program
                                }
                            } else if (componentName.equalsIgnoreCase("inputcontext")) {
                                if (!isIntentFile(filePathText)){
                                    System.err.println("Error: Invalid file specified in <file_path>: " + filePathText);
                                    System.exit(1); // Terminate the program
                                }
                            } else if (componentName.equalsIgnoreCase("outputcontext")) {
                                if (!isIntentFile(filePathText)){
                                    System.err.println("Error: Invalid file specified in <file_path>: " + filePathText);
                                    System.exit(1); // Terminate the program
                                }
                            } else if (componentName.equalsIgnoreCase("simpleentity")) {
                                if(!(mutationName.equals("removeName") | mutationName.equals("changeName"))) {
                                    if (!isSimpleInputFile(filePathText)) {
                                        System.err.println("Error: Invalid file specified in <file_path>: " + filePathText);
                                        System.exit(1); // Terminate the program
                                    }
                                } else {
                                    if (!isEntityFile(filePathText)) {
                                        System.err.println("Error: Invalid file specified in <file_path>: " + filePathText);
                                        System.exit(1); // Terminate the program
                                    }
                                }
                            } else if (componentName.equalsIgnoreCase("chatbot")) {
                                if(!(mutationName.equals("removeIntent") | mutationName.equals("removeEntity"))){
                                    if (!isChatbotFile(filePathText, rootFolderPath)){
                                        System.err.println("Error: Invalid file specified in <file_path>: " + filePathText);
                                        System.exit(1); // Terminate the program
                                    }
                                } else if(mutationName.equals("removeIntent")) {
                                    if (!isIntentFile(filePathText)) {
                                        System.err.println("Error: Invalid file specified in <file_path>: " + filePathText);
                                        System.exit(1); // Terminate the program
                                    }
                                } else {
                                    if (!isEntityFile(filePathText)) {
                                        System.err.println("Error: Invalid file specified in <file_path>: " + filePathText);
                                        System.exit(1); // Terminate the program
                                    }
                                }
                            } else if (componentName.equalsIgnoreCase("parameter")) {
                                if (!isIntentFile(filePathText)) {
                                    System.err.println("Error: Invalid file specified in <file_path>: " + filePathText);
                                    System.exit(1); // Terminate the program
                                }
                            } else if (componentName.equalsIgnoreCase("textaction")) {
                                if (!isIntentFile(filePathText)) {
                                    System.err.println("Error: Invalid file specified in <file_path>: " + filePathText);
                                    System.exit(1); // Terminate the program
                                }
                            }
                        }
                    }
                }
            }
            // If the validation passes, log a success message
            System.out.println("Config.xml validation successful.");
        } catch (Exception e) {
            System.err.println("Error while validating config.xml" + e.getMessage());
            System.exit(1); // Terminate the program
        }
    }

    public static boolean isIntentFile(String filePath) {
        // Check if the path points to a file and not a directory
        File file = new File(filePath);
        if (!file.isFile()) {
            return false;
        }

        // Check if the path contains the directory "intents"
        if (!filePath.contains("intents")) {
            return false;
        }

        // Check if the file is a JSON file
        if (!filePath.toLowerCase().endsWith(".json")) {
            return false;
        }

        // Check if the file name contains "Intent" and not "usersays"
        String fileName = file.getName().toLowerCase();
        if (fileName.contains("usersays")) {
            return false;
        }

        return true;
    }

    public static boolean isEntityFile(String filePath) {
        // Check if the path points to a file and not a directory
        File file = new File(filePath);
        if (!file.isFile()) {
            return false;
        }

        // Check if the path contains the directory "entities"
        if (!filePath.contains("entities")) {
            return false;
        }

        // Check if the file is a JSON file
        if (!filePath.toLowerCase().endsWith(".json")) {
            return false;
        }

        // Check if the file name does not contain "entries"
        String fileName = file.getName().toLowerCase();
        if (fileName.contains("entries")) {
            return false;
        }

        return true;
    }

    public static boolean isChatbotFile(String filePath, String rootPath) {
        // Check if the path points to a file and not a directory
        File file = new File(filePath);
        if (!file.isFile()) {
            return false;
        }

        // Check if the file path matches the expected path for "agent.json"
        if (!filePath.equals(rootPath + File.separator + "agent.json")) {
            return false;
        }

        // Check if the file is a JSON file
        if (!filePath.toLowerCase().endsWith(".json")) {
            return false;
        }

        return true;
    }

    public static boolean isSimpleInputFile(String filePath) {
        // Check if the path points to a file and not a directory
        File file = new File(filePath);
        if (!file.isFile()) {
            return false;
        }

        // Check if the path contains the directory "entities"
        if (!filePath.contains("entities")) {
            return false;
        }

        // Check if the file is a JSON file
        if (!filePath.toLowerCase().endsWith(".json")) {
            return false;
        }

        // Check if the file name contain "entries"
        String fileName = file.getName().toLowerCase();
        if (fileName.contains("entries")) {
            return true;
        }

        return false;
    }
}
