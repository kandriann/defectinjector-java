package it.unimib.input.xml_component;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "mutation")
public class Mutation {
    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "description")
    private String description;

    @XmlElement(name = "enabled")
    private boolean enabled;

    @XmlElement(name = "file_path")
    private List<String> filePaths;

    @XmlElement(name = "output_entire_folder")
    private boolean output_entire_folder;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public List<String> getFilePaths() {
        return filePaths;
    }

    public void setFilePaths(List<String> filePaths) {
        this.filePaths = filePaths;
    }

    public boolean isOutputEntireFolder() {
        return output_entire_folder;
    }

    public void setOutputEntireFolder(boolean output_entire_folder) {
        this.output_entire_folder = output_entire_folder;
    }
}