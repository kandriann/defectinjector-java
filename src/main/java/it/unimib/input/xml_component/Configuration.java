package it.unimib.input.xml_component;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "configuration")
public class Configuration {
    @XmlElement(name = "root_folder")
    private String root_folder;

    @XmlElementWrapper(name = "components")
    @XmlElement(name = "component")
    private List<Component> components;

    public String getRootFolder() {
        return root_folder;
    }

    public void setRootFolder(String root_folder) {
        this.root_folder = root_folder;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    public void addComponent(Component component) {
        components.add(component);
    }
}