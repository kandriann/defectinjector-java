package it.unimib.input.xml_component;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "component")
public class Component {
    @XmlElement(name = "name")
    private String name;

    @XmlElement(name = "mutation_list")
    private List<Mutation> mutation_list;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Mutation> get_mutation_list() {
        return mutation_list;
    }

    public void set_mutation_list(List<Mutation> mutation_list) {
        this.mutation_list = mutation_list;
    }

    public void addOption(Mutation mutation) {
        mutation_list.add(mutation);
    }
}