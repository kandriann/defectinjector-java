package it.unimib.input;

import it.unimib.input.xml_component.Component;
import it.unimib.input.xml_component.Configuration;
import it.unimib.input.xml_component.Mutation;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class XmlReader {

    public static Configuration readConfigurationFromFile(String filePath) {
        try {
            DOMParser parser = new DOMParser();
            parser.setFeature("http://apache.org/xml/features/xinclude", true); // Abilita l'elaborazione degli elementi xi:include
            parser.parse(filePath);

            Document doc = parser.getDocument();
            doc.getDocumentElement().normalize();

            Configuration configuration = new Configuration();
            configuration.setRootFolder(doc.getElementsByTagName("root_folder").item(0).getTextContent());

            NodeList componentList = doc.getElementsByTagName("component");
            List<Component> components = new ArrayList<>();

            for (int i = 0; i < componentList.getLength(); i++) {
                Node componentNode = componentList.item(i);

                if (componentNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element componentElement = (Element) componentNode;

                    Component component = new Component();
                    component.setName(componentElement.getElementsByTagName("name").item(0).getTextContent());

                    NodeList mutation_node_List = componentElement.getElementsByTagName("mutation");
                    List<Mutation> mutation_list = new ArrayList<>();

                    for (int j = 0; j < mutation_node_List.getLength(); j++) {
                        Node mutationNode = mutation_node_List.item(j);

                        if (mutationNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element mutationElement = (Element) mutationNode;

                            Mutation mutation = new Mutation();
                            mutation.setName(mutationElement.getElementsByTagName("name").item(0).getTextContent());
                            mutation.setEnabled(Boolean.parseBoolean(mutationElement.getElementsByTagName("enabled").item(0).getTextContent()));
                            mutation.setDescription(mutationElement.getElementsByTagName("description").item(0).getTextContent());
                            mutation.setOutputEntireFolder(Boolean.parseBoolean(mutationElement.getElementsByTagName("output_entire_folder").item(0).getTextContent()));

                            NodeList filePathNodes = mutationElement.getElementsByTagName("file_path");
                            List<String> filePaths = new ArrayList<>();
                            for (int k = 0; k < filePathNodes.getLength(); k++) {
                                String file_path = filePathNodes.item(k).getTextContent();
                                filePaths.add(file_path);
                            }
                            mutation.setFilePaths(filePaths);

                            mutation_list.add(mutation);
                        }
                    }

                    component.set_mutation_list(mutation_list);
                    components.add(component);
                }
            }

            configuration.setComponents(components);
            return configuration;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}