package it.unimib;

import it.unimib.input.xml_component.Component;
import it.unimib.input.xml_component.Configuration;
import it.unimib.input.xml_component.Mutation;
import it.unimib.modifications.chatbot.ChatbotModifier;
import it.unimib.modifications.context.input_context.InputContextModifier;
import it.unimib.modifications.context.output_context.OutputContextModifier;
import it.unimib.modifications.entity.SimpleEntityModifier;
import it.unimib.modifications.intent.IntentModifier;
import it.unimib.modifications.parameter.ParameterModifier;
import it.unimib.modifications.text_action.TextActionModifier;
import it.unimib.output.FolderWriter;
import it.unimib.output.LogWriter;
import it.unimib.output.ReportWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import static it.unimib.input.XmlReader.readConfigurationFromFile;
import static it.unimib.input.XmlController.validateConfigXml;
import static it.unimib.utils.CommonUtilities.getEnvironmentVariable;

public class Main {
    public static void main(String[] args) throws IOException {
        // Percorso della cartella di output (in cui si scrivono i chatbot mutati) tramite variabile d'ambiente
        String outputFolder = getEnvironmentVariable("OUTPUT_FOLDER_PATH");

        // Validazione del file di configurazione
        validateConfigXml("src/main/resources/config.xml");

        Configuration configuration = readConfigurationFromFile("src/main/resources/config.xml");

        // Ottieni il percorso all'agente DialogFlow
        String input_path = configuration.getRootFolder();
        Path rootFolder = Paths.get(input_path);

        String reportFilePath = ReportWriter.createReport(outputFolder);

        // Scorro i componenti nel file di configurazione
        for (Component component : configuration.getComponents()) {
            String componentName = component.getName();
            List<Mutation> mutation_list = component.get_mutation_list();

            // Verifica le modifiche richieste per il componente corrente
            for (Mutation mutation : mutation_list) {
                String mutationName = mutation.getName();
                boolean isEnabled = mutation.isEnabled();
                List<String> filePaths = mutation.getFilePaths();
                boolean isOutputEntireFolder = mutation.isOutputEntireFolder();

                // Converti le stringhe in oggetti Path
                List<Path> pathsList = new ArrayList<>();
                for (String filePath : filePaths) {
                    Path path = Paths.get(filePath);
                    pathsList.add(path);
                }

                // Esegui la modifica richiesta se l'opzione è abilitata
                if (isEnabled) {

                    for (Path pathJsonFile : pathsList) {
                        // Estrapola il nome del file JSON dal percorso
                        String jsonFileName = pathJsonFile.getFileName().toString();

                        // Creo una cartella con nome univoco e ne restituisco il nome
                        Path outputFolderPath = FolderWriter.createUniqueFolder(mutationName, jsonFileName);

                        // Se l'utente ha chiesto di ricevere in output l'intera cartella, la creo
                        FolderWriter.copyInputFolder(rootFolder, isOutputEntireFolder, outputFolderPath);

                        // Creo il file di log nella cartella con nome univoco
                        LogWriter.setLogFile(outputFolderPath, jsonFileName);

                        try {

                            switch (componentName) {
                                case "Chatbot" -> {
                                    ChatbotModifier chatbotModifier = new ChatbotModifier(pathJsonFile, mutationName, componentName, outputFolderPath, isOutputEntireFolder);
                                    chatbotModifier.modifyChatbot();
                                }
                                case "Intent" -> {
                                    IntentModifier intentModifier = new IntentModifier(pathJsonFile, mutationName, componentName, outputFolderPath, isOutputEntireFolder);
                                    intentModifier.modifyIntents();
                                }
                                case "InputContext" -> {
                                    InputContextModifier contextModifier = new InputContextModifier(pathJsonFile, mutationName, componentName, outputFolderPath, isOutputEntireFolder);
                                    contextModifier.modifyContexts();
                                }
                                case "OutputContext" -> {
                                    OutputContextModifier contextModifier = new OutputContextModifier(pathJsonFile, mutationName, componentName, outputFolderPath, isOutputEntireFolder);
                                    contextModifier.modifyContexts();
                                }
                                case "SimpleEntity" -> {
                                    SimpleEntityModifier simpleEntityModifier = new SimpleEntityModifier(pathJsonFile, mutationName, componentName, outputFolderPath, isOutputEntireFolder);
                                    simpleEntityModifier.modifySimpleEntity();
                                }
                                case "Parameter" -> {
                                    ParameterModifier parameterModifier = new ParameterModifier(pathJsonFile, mutationName, componentName, outputFolderPath, isOutputEntireFolder);
                                    parameterModifier.modifyParameters();
                                }
                                case "TextAction" -> {
                                    TextActionModifier textActionModifier = new TextActionModifier(pathJsonFile, mutationName, componentName, outputFolderPath, isOutputEntireFolder);
                                    textActionModifier.modifyTextAction();
                                }
                            }

                            // Scrivi la riga nel report CSV dopo aver eseguito la modifica
                            ReportWriter.writeReportToCSV(reportFilePath, componentName, mutationName, mutation.getDescription(), outputFolderPath.getFileName().toString(), pathJsonFile.getFileName().toString());
                        } catch (IOException e) {
                            System.err.println("Errore durante l'applicazione delle modifiche: " + e.getMessage());
                        }
                    }
                }
            }
        }
    }
}