package it.unimib.utils;

import it.unimib.output.LogWriter;

import java.util.Random;
import java.util.function.Supplier;

public class IntegerUtilities {
    private static final Random random = new Random();

    public static Supplier<Integer> getRandomIntegerGenerator(int currentValue, String attributeName) {
        Supplier<Integer>[] generators = new Supplier[]{
                () -> setToZero(),
                () -> setToNegative(currentValue),
                () -> addRandomInteger(currentValue, attributeName),
                () -> subtractRandomInteger(currentValue, attributeName)
        };

        int randomIndex = random.nextInt(generators.length);
        return generators[randomIndex];
    }

    public static Integer setToZero() {
        LogWriter.writeLog("Il valore verrà settato a 0");
        return 0;
    }

    public static Integer setToNegative(int n) {
        LogWriter.writeLog("Il valore attuale verrà settato ad un valore negativo");
        return -n;
    }

    public static Integer addRandomInteger(int n, String attribute) {
        LogWriter.writeLog("Al valore attuale verrà aggiunto un valore casuale");
        int bound = getBoundForAttribute(attribute);
        return n + random.nextInt(bound+1);
    }

    public static Integer subtractRandomInteger(int n, String attribute) {
        LogWriter.writeLog("Al valore attuale verrà sottratto un valore casuale");
        int bound = getBoundForAttribute(attribute);
        return n - random.nextInt(bound+1);
    }

    private static int getBoundForAttribute(String attribute) {
        if (attribute.equals("lifespanCount")) {
            return 999;
        } else { // This can only be priority
            return 5;
        }
    }
}
