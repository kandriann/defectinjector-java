package it.unimib.utils;

import it.unimib.output.LogWriter;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class LanguageUtilities {
    private static final Random random = new Random();

    // Definizione dell'elenco globale di abbreviazioni delle lingue
    private static final List<String> languageAbbreviations = List.of("af", "am", "az", "bg", "bn",
                                                                      "bs", "ca", "ceb", "co", "cs",
                                                                      "cy", "da", "de", "el", "en",
                                                                      "eo", "et", "eu", "fi", "fil",
                                                                      "fr", "fy", "ga", "gd", "gl",
                                                                      "gu", "ha", "hi", "hmn", "hr",
                                                                      "ht", "hu", "hy", "id", "ig",
                                                                      "is", "ja", "jv", "ka", "kk",
                                                                      "km", "kn", "ko", "ku", "ky",
                                                                      "la", "lb", "lt", "lv", "mg",
                                                                      "mi", "mk", "ml", "mn", "mr",
                                                                      "ms", "mt", "ne", "nl", "no",
                                                                      "ny", "or", "pa", "pl", "pt",
                                                                      "pt-br", "ro", "ru", "rw", "si",
                                                                      "sk", "sl", "sm", "sn", "so",
                                                                      "sq", "sr", "st", "su", "sv",
                                                                      "sw", "ta", "te", "tg", "th",
                                                                      "tk", "tr", "tt", "uk", "uz",
                                                                      "vi", "xh", "yo", "zh-cn", "zh-kh",
                                                                      "zh-tw", "zu");
    public static Supplier<String> getRandomLanguageGenerator() {
        Supplier<String>[] generators = new Supplier[] {
                LanguageUtilities::generateRandomString,
                LanguageUtilities::emptyLanguage,
                LanguageUtilities::getRandomLanguageAbbreviation
        };

        int randomIndex = random.nextInt(generators.length);
        return generators[randomIndex];
    }

    public static String emptyLanguage() {
        LogWriter.writeLog("The value with which the language will be replaced is an empty string");
        return "";
    }

    public static String generateRandomString() {
        LogWriter.writeLog("The value with which the language will be replaced is a random string");

        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder(6);
        for (int i = 0; i < 4; i++) {
            int randomIndex = random.nextInt(chars.length());
            char randomChar = chars.charAt(randomIndex);
            sb.append(randomChar);
        }
        return sb.toString();
    }

    public static String getRandomLanguageAbbreviation() {
        LogWriter.writeLog("The value with which the language will be replaced is a random language abbreviations");

        int randomIndex = new Random().nextInt(languageAbbreviations.size());
        return languageAbbreviations.get(randomIndex);
    }
}
