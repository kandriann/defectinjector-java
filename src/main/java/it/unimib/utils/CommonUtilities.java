package it.unimib.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import it.unimib.output.LogWriter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class CommonUtilities {

    // Recupero la variabile d'ambiente e controllo che sia valorizzata
    public static String getEnvironmentVariable(String variableName) {
        Map<String, String> env = System.getenv();
        String value = env.get(variableName);

        if (value == null || value.isEmpty()) {
            System.err.println("Error: The " + variableName + " environment variable is not set.");
            System.exit(1); // Terminate the program
        }

        return value;
    }

    // Leggo il file json di interesse per poter svolgere le mutazioni necessarie
    public static JsonObject readJsonFile(Path path){
        String jsonString;
        try {
            jsonString = new String(Files.readAllBytes(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return JsonParser.parseString(jsonString).getAsJsonObject();
    }

    // Leggo il l'array json di interesse per poter svolgere le mutazioni necessarie
    public static JsonArray readJsonArray(Path path) {
        String jsonString;
        try {
            jsonString = new String(Files.readAllBytes(path));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return JsonParser.parseString(jsonString).getAsJsonArray();
    }

    public static void logAttributeInfo(JsonObject jsonObject, String attributeName) {
        if (jsonObject.has(attributeName)) {
            String attributeValue = jsonObject.get(attributeName).toString();
            LogWriter.writeLog("Attribute '" + attributeName + "' exists with value: " + attributeValue);
        } else {
            LogWriter.writeLog("Attribute '" + attributeName + "' does not exist.");
        }
    }

}
