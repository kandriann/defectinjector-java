package it.unimib.utils;

import it.unimib.output.LogWriter;

import java.util.Random;
import java.util.function.Supplier;

public class StringUtilities {
    private static final Random random = new Random();

    public static Supplier<String> getRandomStringGenerator() {
        Supplier<String>[] generators = new Supplier[] {
                StringUtilities::generateRandomString,
                StringUtilities::emptyString,
                StringUtilities::generateRandomSpecialCharsString,
                StringUtilities::generateRandomLongString
        };

        int randomIndex = random.nextInt(generators.length);
        return generators[randomIndex];
    }

    public static String generateRandomString() {
        LogWriter.writeLog("The string with which the value will be replaced is a random string");
        return generateRandomString(10); // Default length is 10 characters
    }

    public static String generateRandomSpecialCharsString() {
        String specialChars = "!@#$%^&*()_-+=<>?/[]{}|";
        StringBuilder sb = new StringBuilder(10);
        for (int i = 0; i < 10; i++) {
            int randomIndex = random.nextInt(specialChars.length());
            char randomChar = specialChars.charAt(randomIndex);
            sb.append(randomChar);
        }
        LogWriter.writeLog("The string with which the value will be replaced is a random string composed of special characters");
        return sb.toString();
    }

    public static String generateRandomLongString() {
        LogWriter.writeLog("The string with which the value will be replaced is a long random string");
        return generateRandomString(100); // Generate a longer string
    }

    public static String emptyString() {
        LogWriter.writeLog("The string with which the value will be replaced is an empty string");
        return "";
    }

    public static String generateRandomString(int length) {
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(chars.length());
            char randomChar = chars.charAt(randomIndex);
            sb.append(randomChar);
        }
        return sb.toString();
    }
}
