# Mutabot

This repository contains the project for injecting defect into chatbots developed with Dialogflow.


## Project Description

Mutation testing is a technique aimed at assessing the effectiveness of test suites by seeding artificial faults into programs. Although available for many platforms and languages, no mutation testing tool is currently available for conversational chatbots, which represent an increasingly popular solution to design systems that can interact with users through a natural language interface. Note that since conversations must be explicitly engineered by the developers of conversational chatbots, these systems are exposed to specific types of faults not supported by existing mutation testing tools. This repository contains MutaBot, a mutation testing tool for conversational chatbots. MutaBot addresses mutations at multiple levels, including conversational flows, intents, and contexts. We designed the tool to potentially target multiple platforms, while we implemented initial support for Google Dialogflow chatbots. We assessed the tool with three Dialogflow chatbots and test cases generated with Botium, revealing weaknesses in the test suites.


## Branches

- `main`: This is the main branch and should be used when you want to utilize the Mutabot with all its stable and fully functional features. The `main` branch contains the latest release of the project.

- `develop`: The `develop` branch is used for ongoing development work and may contain experimental or unfinished features. It is not recommended for regular usage of the Mutabot. Stick to the `main` branch for stable functionality.


## Structure of the Repository

- `src/main/java/`: This directory contains the source code for the project. It includes the Java classes used for generating mutations and performing checks to verify the integrity of xml files.

- `src/main/config/`: This directory contains the XML files that identify each of the chatbot elements and the related defects that the user can choose to apply.

- `src/main/resources/`: This directory contains the file "config.xml," which collects all the XML files that transpose the mutations desired by the user.


## Configuration

- You need to configure the Java development environment to run the project.

- Create two environment variables locally:
  - `INPUT_FOLDER_PATH`: specifies the path from which the Mutabot will read the chatbot given as input;
  - `OUTPUT_FOLDER_PATH`: specifies the path where the Mutabot will write the mutated chatbots to output.

- Before running the project, you need to configure the XML files (contained in `src/main/config/`) that represent the chatbot components you want to mutate. Each XML mutation file is structured as follows:

  - `<name>` (predefined): Provides a unique name for the mutation, so far the following are implemented: Chatbot, InputContext, Intent, OutputContext, Parameter, SimpleEntity, TextAction.
  - `<description>` (predefined): Describes the purpose of this mutation.
  - `<enabled>` (default: `false`, editable): Set this tag to `true` if you want to apply this mutation, or keep it as `false` to skip it.
  - `<file_path>`: Specify the path to the JSON file that represents the chatbot component you want to mutate. If the path to a json file does not represent the chatbot element described by the <name> tag, the programme will terminate its execution by returning an error.
  - `<output_entire_folder>`: Set this tag to `true` if you want to receive the entire folder of the chatbot with the mutated file inside, or `false` to receive only the mutated JSON file.

- In addition, the config.xml file (contained at the path 'src/main/resources/') must also be configured before running the project. This mutation XML file is structured as follows:
  - `<configuration>` (predefined): use of the namespace 'xi' to enable functionality for external inclusion of XML resources (XInclude) within the file.
  - `<root_folder>`: specify the path to the folder of the chatbot that you want to mutate.
  - `<components>` (predefined): compiled to include xml files, each representing an element of a Dialogflow chatbot, for which the MutaBot provides at least one mutation.

Once you have configured these XML files to represent the mutations you want to apply, you can proceed with the execution of the project as explained in the "Execution" section.



## Execution

To execute the project, you need to follow these steps:

1. Compile the xml files contained in the `src/main/config/` directory to indicate which mutations you want to apply;

2. Run the java class `src/main/java/en/unimib/Main.java`.


## Result

In this section, we present a clear organization of the results obtained through the Mutation Injection process on chatbots.

### Structure of Mutations

For each mutation requested by the user, the system generates a mutated version of the original chatbot. These mutated versions are specifically organized:

- **Mutation Folder**: Each mutated version is contained within a folder whose name is structured as follows: [Timestamp] - [Modified JSON File Name] - [Type of Mutation Applied]. This format provides clear identification of the mutations made.

- **Log Files**: Within each mutation folder, there is a text file that serves as a log of operations during the mutation process. This file records all actions performed during the specific mutation.

### Mutations Report

For an aggregated view of the mutations made during a specific run of Mutation Testing, all results are collected in a CSV file. This file provides a comprehensive and structured report of all applied mutations, allowing detailed analysis of the results obtained.


## Contacts

For questions or clarification:

- Name: [Michael Ferdinando Urrico]
- Email: [m.urrico@campus.unimib.it]


- Name: [Diego Clerissi]
- Email: [diego.clerissi@unimib.it]


- Name: [Leonardo Mariani]
- Email: [leonardo.mariani@unimib.it]
